/**
 * Created by Ruslan on 3/30/2016.
 */
var express = require('express'),
    app = express(),
    path = require('path'),
    port = "8000";

app.use(express.static(path.join(__dirname, 'app')));
// viewed at http://localhost:8000
app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname + '/app/index.html'));
});
app.set('port', port || process.env.PORT);
app.listen(port, function () {
    console.log('Example app listening on port '+port);
});

//function(req, res){
//    res.render('index');
//}