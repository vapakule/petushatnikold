'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
    'ngRoute',
    'ngCookies',
    'ngMaterial',
    'myApp.view1',
    'myApp.register',
    'myApp.version'
])
.config(['$routeProvider', '$locationProvider','$httpProvider','$mdThemingProvider', function ($routeProvider, $locationProvider,$httpProvider,$mdThemingProvider) {
    //$routeProvider.otherwise({redirectTo: '/view1'});
    $locationProvider.html5Mode(true);

    $routeProvider
    .when('/', {
        templateUrl: 'views/home/homeView.html',
        controller: 'HomeController'
        // access : {allowAnonymous : true}
    })
    .when('/feed', {
        templateUrl: 'views/feed/feedView.html',
        controller: 'FeedController'
        // access : {allowAnonymous : true}
    })
    .otherwise({
        redirectTo: '/'
    });

    //THEMES
    var customBlueMap = 		$mdThemingProvider.extendPalette('light-blue', {
        'contrastDefaultColor': 'light',
        'contrastDarkColors': ['50'],
        '50': 'ffffff'
    });
    $mdThemingProvider.definePalette('customBlue', customBlueMap);
    $mdThemingProvider.theme('default')
        .primaryPalette('customBlue', {
            'default': '500',
            'hue-1': '50'
        })
        .accentPalette('pink');
    $mdThemingProvider.theme('input', 'default')
        .primaryPalette('grey')
}]);

