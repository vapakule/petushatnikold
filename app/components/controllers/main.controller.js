/**
 * Created by Ruslan on 3/30/2016.
 */
'use strict';
angular.module('myApp')
    .controller('MainController', function ($scope, $rootScope, $timeout, $mdSidenav, $mdDialog,$mdBottomSheet, $log, $location) {
        //SideBar
        $scope.toggleSidenav = function(menuId) {
            $mdSidenav(menuId).toggle();
        };
        $scope.toggleLeft = buildDelayedToggler('left');
        $scope.toggleRight = buildToggler('right');
        $scope.isOpenRight = function(){
            return $mdSidenav('right').isOpen();
        };
        $scope.go = function ( path ) {
            console.log("go",path);
            $location.path( path );
        };
        /**
         * Supplies a function that will continue to operate until the
         * time is up.
         */
        function debounce(func, wait, context) {
            var timer;
            return function debounced() {
                var context = $scope,
                    args = Array.prototype.slice.call(arguments);
                $timeout.cancel(timer);
                timer = $timeout(function() {
                    timer = undefined;
                    func.apply(context, args);
                }, wait || 10);
            };
        }
        /**
         * Build handler to open/close a SideNav; when animation finishes
         * report completion in console
         */
        function buildDelayedToggler(navID) {
            return debounce(function() {
                $mdSidenav(navID)
                    .toggle()
                    .then(function () {
                        $log.debug("toggle " + navID + " is done");
                    });
            }, 200);
        }
        function buildToggler(navID) {
            return function() {
                $mdSidenav(navID)
                    .toggle()
                    .then(function () {
                        $log.debug("toggle " + navID + " is done");
                    });
            }
        }
        //Header
        $scope.header = {
        };
        //SideNav
        $scope.menu = [
            {
                link : '',
                title: 'Dashboard',
                icon: 'dashboard'
            },
            {
                link : '',
                title: 'Friends',
                icon: 'group'
            },
            {
                link : '',
                title: 'Messages',
                icon: 'message'
            }
        ];
        $scope.admin = [
            {
                link : '',
                title: 'Trash',
                icon: 'delete'
            },
            {
                link : 'showListBottomSheet($event)',
                title: 'Settings',
                icon: 'settings'
            }
        ];

        //Modals
        $scope.getHooked = function(ev) {
            $mdDialog.show({
                    controller: DialogController,
                    templateUrl: 'shared/dialogs/login.register/loginRegisterSwitch.html',
                    parent: angular.element(document.body),
                    targetEvent: ev,
                    clickOutsideToClose:true
                })
                .then(function(answer) {
                    $scope.status = 'You said the information was "' + answer + '".';
                }, function() {
                    $scope.status = 'You cancelled the dialog.';
                });
        };
        function DialogController($scope, $mdDialog) {
            $scope.hide = function() {
                $mdDialog.hide();
            };
            $scope.cancel = function() {
                $mdDialog.cancel();
            };
            $scope.answer = function(answer) {
                $mdDialog.hide(answer);
            };
        }
    });