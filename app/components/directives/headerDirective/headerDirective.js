/**
 * Created by Ruslan on 3/31/2016.
 */
angular.module('myApp').directive('appHeader',header);
function header () {
    return {
        restrict: 'EA',
        replace: true,
        controller: 'MainController',
        templateUrl: 'components/directives/headerDirective/headerView.html'
    };
}