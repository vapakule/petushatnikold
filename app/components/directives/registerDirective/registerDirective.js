angular.module('myApp').directive('registerDirective', ['CustomerService', function (CustomerService) {
  return {
    restrict: 'E',
    templateUrl: 'components/directives/registerDirective/registerDirective.html',
    scope: {},
    link: function (scope, elem, attrs) {
      
      // scope.form = {
      //   name : "",
      //   email: "",
      //   password: "",
      //   password2: ""
      // };
      
      scope.passwordPattern = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/
      
      

      scope.emailPattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

// 
//       ngModel.$validators.email = function (email) {
//         var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
//         return re.test(email);
//       }


      scope.send = function (data) {
        CustomerService.registerCustomer(data)
          .then(function successCallback(response) {
            console.log('success');
            console.log(response);

          }, function errorCallback(response) {
            alert(response.data);
            console.log('fail');
            console.log(response);

          });
      };
      
      

      //       scope.login = function () {
      //         console.log("logging in")
      //         var obj = {
      //           "email": "string11@gmail.com",
      //           "password": "Qwe123",
      //           "rememberMe": false
      //         };
      // 
      //         var headerz = {
      //           'Access-Control-Allow-Methods': 'POST, GET, OPTIONS, PUT',
      //           'Content-Type': 'application/json',
      //           'Accept': 'application/json',
      //           'Access-Control-Allow-Credentials': 'true'
      //         };
      // 
      //         var headers = {
      //           'Authorization': 'Basic d2VudHdvcnRobWFuOkNoYW5nZV9tZQ==',
      //           'Access-Control-Allow-Credentials': 'true',
      //           'Accept': 'application/json;odata=verbose'
      //         }
      //         $http({
      //           method: 'POST',
      //           url: 'http://kurjatnik.azurewebsites.net/api/User/Login',
      //           headers: headers,//{'Access-Control-Allow-Credentials': 'true'},
      //           withCredentials: true,
      //           data: obj
      //         }).then(function successCallback(response) {
      // 
      //           console.log('login success');
      //           console.log(response);
      // 
      // 
      //         }, function errorCallback(response) {
      //           console.log('login fail');
      //           console.log(response);
      // 
      //         });
      //       }
      // 
      //       scope.isLoggedin = function () {
      // 
      //         $http({
      //           method: 'GET',
      //           url: "http://kurjatnik.azurewebsites.net/api/Test/IsLoggedIn",
      // 
      //           withCredentials: true
      //         }).then(function successCallback(response) {
      //           console.log('login check success');
      //           console.log(response);
      // 
      //         }, function errorCallback(response) {
      //           console.log('login check fail');
      //           console.log(response);
      //         });
      //         console.log(" login check");
      //       }
      // 
      //       scope.logout = function () {
      //         $http({
      //           method: 'POST',
      //           url: "http://kurjatnik.azurewebsites.net/api/User/Logout",
      //           withCredentials: true
      //         }).then(function successCallback(response) {
      //           console.log('logout  success');
      //           console.log(response);
      // 
      //         }, function errorCallback(response) {
      //           console.log('logout  fail');
      //           console.log(response);
      //         });
      //       }
    }
  }
}]);