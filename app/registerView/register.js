'use strict';

angular.module('myApp.register', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/register', {
    templateUrl: 'registerView/register.html',
    controller: 'registerCtrl'
  });
}])

.controller('registerCtrl', [function() {
console.log("imalive")
}]);