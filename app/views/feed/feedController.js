/**
 * Created by Ruslan on 3/31/2016.
 */
'use strict';
angular.module('myApp')
    .controller('FeedController', function ($scope,$http) {
        $http.get('test.json').success(function(data) {
            $scope.data = data.entries;
            console.log(data.entries);
        });
    });